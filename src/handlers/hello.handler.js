const { Todo } = require('../model')

const helloFromRoot = async (req, res) => {
  return res.status(200).json({
    message: "Hello from root!",
  });
}

const helloFromPath = async (req, res) => {
  return res.status(200).json({
    message: "Hello from path!",
  });
}

const createTodo = async (req, res) => {
  const todo = await Todo.create(req.body)
  return res.status(201).json({
    message: "Created",
    todo
  });
}

module.exports = {
  helloFromRoot,
  helloFromPath,
  createTodo
}
