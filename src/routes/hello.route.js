const { Router } = require('express');
const { helloHandler } = require('../handlers')

const router = Router();

router.route('/').get(helloHandler.helloFromRoot).post(helloHandler.createTodo)

router.route('/hello').get(helloHandler.helloFromPath)

module.exports = router;