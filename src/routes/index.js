const { Router } = require('express');
const helloRoute = require('./hello.route')

const router = Router();

router.use('/', helloRoute);

module.exports = router;