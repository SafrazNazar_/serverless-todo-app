const serverless = require("serverless-http");
const express = require("express");
const routes = require('./routes')
const bodyParser = require('body-parser')
const helmet = require('helmet')
require('./config/db')

const app = express();

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(helmet())

app.use(routes)

app.use((req, res, next) => {
    return res.status(404).json({
      error: "Not Found",
    });
  });

module.exports.handler = serverless(app);