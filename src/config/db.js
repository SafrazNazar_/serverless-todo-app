// const mongoose = require('mongoose')
// const connection = {}

// module.exports = async () => {
//   if (connection.isConnected) {
//     console.log('=> using existing database connection')
//     return
//   }

//   console.log('=> using new database connection')
//   const db = await mongoose.connect(process.env.DB_URL, {dbName: process.env.DB_NAME})
//   connection.isConnected = db.connections[0].readyState
// }

const mongoose = require('mongoose')
mongoose.connect(process.env.DB_URL, {dbName: process.env.DB_NAME})